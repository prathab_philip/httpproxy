import requests
import time

class BaseCheck():
    def __init__(self, url):
        self.http_proxy  = "http://localhost:8001"
        self.proxyDict = {
                      "http"  : self.http_proxy,
                    }
        self.url = url

    def __str__(self):
        return str(self.url)
 
    def getrequests(self):
        return requests.get(self.url,proxies=self.proxyDict)
        
if __name__ == '__main__':
    urls = ['http://www.cg.nic.in/rajnandgaon/UserSearch.aspx',
            'http://jandarshan.cg.nic.in/downloads.htm',
            'http://checklist.onlineflora.cn/',
            'http://checklist.onlineflora.cn/taxonomy/term/107236',
            'http://checklist.onlineflora.cn/gallery',
            'http://checklist.onlineflora.cn/glossary']

    for url in urls:
        test=BaseCheck(url)
        res = test.getrequests()
        if not (res.status_code == 200):
            print "Malware url", url
        else:
            print "Safe Site" , url
