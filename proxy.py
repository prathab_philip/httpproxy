import SocketServer
import SimpleHTTPServer
import sys
import urllib
import requests
import ConfigParser
from urlparse import urlparse 

class Proxy(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):
        print "THE URL!..." , self.path
        obj = urlparse(self.path)
        netloc = obj.netloc

        # To Do 
        # rest call to see if the url is good by passing the host, port and
        # path
        if  obj.path == '/':
            path = '/NO_PATH'
        else:
            path = obj.path

        rq = requests.get('http://%s:%s/urlinfo/1/%s%s' %(REST_SERVER,REST_PORT,netloc,path))

        if "Safe Site" in rq.content:
            self.copyfile(urllib.urlopen(self.path), self.wfile)
        else: 
            self.send_error(403, "Potentially a Malware Site")

if __name__ == '__main__':  

    config = ConfigParser.ConfigParser()
    config.read('setup.ini')

    PORT = int(config.get('proxy', 'proxy_port'))
    REST_SERVER = config.get('rest_service', 'rest_server')
    REST_PORT = config.get('rest_service', 'rest_port')

    httpd = SocketServer.ForkingTCPServer(('', PORT), Proxy)
    print "serving at port", PORT
    httpd.serve_forever()
