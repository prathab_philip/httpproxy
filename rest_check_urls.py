import ConfigParser
from flask import Flask, redirect, url_for, request
from flask_cors import CORS
from db_service import ConnDB

app = Flask(__name__)
CORS(app, supports_credentials=True)


@app.route('/update_table/<string:url>')
def update_table(mal_url,methods = ['POST']):
    mal_url = request.args.get('summary', None) 
    return ' Table is updated with given url '


@app.route('/urlinfo/<int:id>/<string:domain>/<path:urlpath>')
def urlinfo(id,domain,urlpath, methods = ['GET'] ):
    """
       This function is to query the the bd_service with the host, port 
       and path fron the incoming GET request and check if the url is 
       malware 
    """
    
    hostnport = domain.split(':')
    if len(hostnport) == 2:
       host = hostnport[0]
       port = hostnport[1]
    else:
       host = hostnport[0]
       port = 80 
      
    if  'NO_PATH' in urlpath :
       urlpath = 'NULL'

    if dbobj.fetch_db(host, port, urlpath):
        result = "It's not safe to access the site, Potential Malware"
         
    else: 
        result = "Safe Site"
    return result 

if __name__ == '__main__':

    config = ConfigParser.ConfigParser()
    config.read('setup.ini')

    PORT = int(config.get('rest_service', 'rest_port'))

    dbobj = ConnDB()
    app.run(host="0.0.0.0", port=PORT, debug=True)
