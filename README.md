#  Http Proxy
## Package Dependencies
### PIP
```
pip install flask 
```

### Components:
proxy.py : Depends on the setup.ini file (eg: proxy run on port 8001 )

rest_check_urls.py Depends on the setup.ini file(eg: rest service runs on port 5010 )

db_service.py: MySql server  to  installed on a linux system and provide the correct information in the db_config.ini 

```
setup.ini
[proxy]
proxy_server=localhost
proxy_port=8001

[rest_service]
rest_server=localhost
rest_port=5010 
--------------------------------------------
db_conf.ini
[mysql]
host=localhost
user=root
passwd=abcd1234
db=urldb
```
Note : 
    It is assumed the all the above components are running on a same Linux system. 
    Create a database urldb, and also create a table in the name malwareurl
```
mysql> select * from  malwareurl ;
+--------------------------+------+-----------------------------+
| host                     | port | path                        |
+--------------------------+------+-----------------------------+
| checklist.onlineflora.cn |   80 | classification/16          |
| somehttp.com             | 8080 | somepath/somequery          |
| malware.com              | 8080 | malware?search=malquery    |
| malwareone.com           | 8080 | malwareone?search=malquery |
| malwareten.com           | 8080 | malwareten?search=malquery |
| checklist.onlineflora.cn |   80 | taxonomy/term/107236       |
| checklist.onlineflora.cn |   80 | gallery                    |
| checklist.onlineflora.cn |   80 | glossary                   |

```
### To Run Each Components:
    1.	Open a linux command line terminal and run
```
    Python proxy.py
```
    2.	Open another linux command line terminal and run
```
    Python rest_check_urls.py
```

### Test Sample:
The URLS user for the test are : the late two urls are expected to fails

```
    urls = ['http://www.cg.nic.in/rajnandgaon/UserSearch.aspx',
            'http://jandarshan.cg.nic.in/downloads.htm',
            'http://checklist.onlineflora.cn/',
            'http://checklist.onlineflora.cn/taxonomy/term/107236',
            'http://checklist.onlineflora.cn/gallery',
            'http://checklist.onlineflora.cn/glossary']
```

``` 
python test_proxy.py
Safe Site http://www.cg.nic.in/rajnandgaon/UserSearch.aspx
Safe Site http://jandarshan.cg.nic.in/downloads.htm
Safe Site http://checklist.onlineflora.cn/
Safe Site http://checklist.onlineflora.cn/taxonomy/term/107236
Malware url http://checklist.onlineflora.cn/gallery
Malware url http://checklist.onlineflora.cn/glossary
```

##Give some thought to the following:
###The size of the URL list could grow infinitely, how might you scale this beyond the memory capacity of the system? Bonus if you implement this.
```
if the query match multiple records , i can  do a fetch limit  in the sql query. 
```
###The number of requests may exceed the capacity of this system, how might you solve that? Bonus if you implement this.

###What are some strategies you might use to update the service with new URLs? Updates may be as much as 5 thousand URLs a day with updates arriving every minute
```
i woud have the URLs in a message queues like rabitmq or kafka and read it from the queue so that the urls are not lost at any point.
```