import MySQLdb
import ConfigParser


class ConnDB():
    """ 
    This is a class to  connect to MySql database and fetch the urls  from DB
    """

    def __init__(self): 
        """
        This consructor is to get the db paramaters from db_conf.ini
        and create a connection object.
        """

        config = ConfigParser.ConfigParser()
        config.read('db_conf.ini')
        self.db = MySQLdb.connect(host=config.get('mysql', 'host'),  # your host 
                     user=config.get('mysql', 'user'),       # username
                     passwd= config.get('mysql', 'passwd'),# password
                     db=config.get('mysql', 'db'))   # name of the database
        self.cur = self.db.cursor()


    def fetch_db(self, host, port, path):
        """
        This method fetches the matching records from the db 
        with the provided inputs
 
        Parameters: 
        host: The domain name of a url (eg checklist.onlineflora.cn)
        port: The port number of the url , if null defaults to 80
        path: The path would be the remaining of a url
        """  

        if path is 'NULL':  
            query = "SELECT * FROM malwareurl WHERE port = %s \
                 AND host = '%s' AND path IS NULL" %(port, host)
        else:
            query = "SELECT * FROM malwareurl WHERE port = %s \
                 AND host = '%s' AND path = '%s'" %(port, host, path)
        
        self.cur.execute(query)
        records = self.cur.fetchall()
        if not (len(records) == 0):
          for row in records :
            print row[0], " ", row[1] ," ", row[2]
            flag = True
        else:
            flag = False

        return bool(flag)


    def update_db(self, host, port, path):
        """
        This method is to update the table with new malware url records
        Parameters: 
        host: The domain name of a url (eg checklist.onlineflora.cn)
        port: The port number of the url , if null defaults to 80
        path: The path would be the remaining of a url
        """
        
        ins_cmd = "INSERT INTO malwareurl(host, port,  path) \
                   VALUES ('%s', %s, '%s')" %(host, port, path)
        
        self.cur.execute(ins_cmd)
        self.db.commit()  

if __name__ == "__main__":

    o = ConnDB()
    #o.fetch_db('checklist.onlineflora.cn', 80, 'NULL')
    o.update_db('malwareten.com', 8080, r"/malwareten?search=malquery")
